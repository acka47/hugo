---
title: This is a test
date: 2018-01-29
---

# Dies ist eine Test-OER

foo bar

## Lizenz

![Creative Commons License](https://i.creativecommons.org/l/by/4.0/88x31.png) This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

<script type="application/ld+json">
{
   "@context":"http://schema.org",
   "type":"CreativWork",
   "keywords":[
      "OER",
      "Mathe"
   ],
   "url":"https://example.org/oer/",
   "license": "http://creativecommons.org/licenses/by/4.0/",
   "author":{
      "@type":"Person",
      "name":"Tobias Steineroer",
      "sameAs":[
         "https://oerworldmap.org/resource/urn:uuid:0e3d180e-73c3-42ec-8971-aabe20d4fc50",
         "https://orcid.org/0000-0002-3158-3136",
         "https://twitter.com/cmplxtv_studies"
      ]
   }
}
</script>

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.